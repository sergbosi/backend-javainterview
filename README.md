# backend-javaInterview

## Prerrequisites
1. git
2. jdk 1.8
3. docker  Optional

On any system with the JDK installed, running the application from the command line should be as simple as './gradlew bootRun'. 

## For Docker deploy you can deploy it to docker using the following commands:
open a terminal

1. git clone https://sergbosi@bitbucket.org/sergbosi/backend-javainterview.git 
 - go to the directory where yo clonned the repository , then open a terminal and execute:
2. docker build -t spring partnerships-offshore-challenge-master
3. docker run --publish 8080:8080 --name spring_cont spring -d
 
 then checkout the front-end repository  [link](https://bitbucket.org/sergbosi/frontend-angularinterview/src/master/)

 
