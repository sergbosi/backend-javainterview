/* Populate tables */

INSERT INTO venues (name, city, state) VALUES ('Wrigley Field', 'CHICAGO', 'IL' );
INSERT INTO venues (name, city, state) VALUES ('CHICAGO VENUE', 'ATLANTA', 'GE' );
INSERT INTO venues (name, city, state) VALUES ('CHICAGO VENUE', 'NEW YORK', 'NY' );

INSERT INTO events (name, date, venue_id) VALUES ('Chicago White Sox at Chicago Cubs',  NOW(), 1);
 INSERT INTO events (name, date,venue_id) VALUES ('Chicago White Sox at Chicago Cubs',  NOW(), 2);
 INSERT INTO events (name, date,venue_id) VALUES ('Chicago White Sox at Chicago Cubs',  NOW(), 3);
 
 