package com.vivid.partnerships.interview.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.vivid.partnerships.interview.model.Event;
import com.vivid.partnerships.interview.model.EventRowMapper;
import com.vivid.partnerships.interview.model.dao.EventDaoI;

import java.util.List;

@Service
public class EventService implements EventServiceI {
 
	
 
	private final EventDaoI dao;
	
	
	 @Autowired
	    public EventService(EventDaoI dao) {
	        this.dao = dao;
	    }
	 @Override
	 @Transactional(readOnly = true)
	  public List<Event> getEvents() {
	         return  dao.findAll();
	    }
	 
	 
	@Override
	 @Transactional
	public Event saveEvent(Event ev) {
	 
		return dao.save(ev);
	}
	 
	 
	
    
    
}
