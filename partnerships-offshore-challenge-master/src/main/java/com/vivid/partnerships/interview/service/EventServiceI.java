package com.vivid.partnerships.interview.service;

import java.util.List;

import com.vivid.partnerships.interview.model.Event;

public interface EventServiceI {
	public List<Event> getEvents();
	public Event saveEvent(Event ev);
}
